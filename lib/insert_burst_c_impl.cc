/* -*- c++ -*- */
/*
 * Copyright 2023 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "insert_burst_c_impl.h"
#include <gnuradio/io_signature.h>

namespace gr {
namespace cdc {

insert_burst_c::sptr insert_burst_c::make() {
    return gnuradio::make_block_sptr<insert_burst_c_impl>();
}

insert_burst_c_impl::insert_burst_c_impl()
    : gr::sync_block("insert_burst_c",
                     gr::io_signature::make(0, 0, 0),
                     gr::io_signature::make(1, 1, sizeof(gr_complex))),
      d_length(0),
      d_offset(0)
{
    message_port_register_in(pmt::mp("burst"));
}

insert_burst_c_impl::~insert_burst_c_impl() {}

int insert_burst_c_impl::work(int noutput_items,
                              gr_vector_const_void_star &input_items,
                              gr_vector_void_star &output_items) {
    auto out = static_cast<gr_complex *>(output_items[0]);
    int count = 0;

    // check for new burst
    if (d_length == 0) {
        pmt::pmt_t msg(delete_head_nowait(pmt::mp("burst")));
        if (msg.get() != NULL) {
            d_burst  = pmt::cdr(msg);
            d_length = pmt::blob_length(d_burst) / sizeof(gr_complex);
            d_offset = 0;
        }
    }

    // output burst if we have one
    if (d_length > 0) {
        size_t io(0);
        const gr_complex *vec = c32vector_elements(d_burst, io);
        count = std::min(noutput_items, d_length - d_offset);

        memcpy(out, vec, count*sizeof(gr_complex));
        
        out += count;
        d_offset += count;
        if (d_offset >= d_length) {
            d_burst.reset();
            d_length = 0;
        }
    }

    // output zeros for any remaining samples
    memset((void *)out, 0, (noutput_items - count)*sizeof(gr_complex));

    return noutput_items;
}

} /* namespace cdc */
} /* namespace gr */
