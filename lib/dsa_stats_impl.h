/* -*- c++ -*- */
/*
 * Copyright 2023 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CDC_DSA_STATS_IMPL_H
#define INCLUDED_CDC_DSA_STATS_IMPL_H

#include <gnuradio/cdc/dsa_stats.h>
#include <chrono>

using std::chrono::time_point;
using std::chrono::steady_clock;

namespace gr {
namespace cdc {

class dsa_stats_impl : public dsa_stats
{
private:
    std::shared_ptr<gr::thread::thread> d_thread;
    bool d_finished;
    long d_period;
    bool d_primary;
    double d_tput_offered;

    // stat variables
    gr::thread::mutex d_mtx;
    time_point<steady_clock> d_start_tp;
    time_point<steady_clock> d_last_tp;
    double d_bytes_new = 0.0;
    double d_bytes = 0.0;
    double d_tput_remote = 0.0;

    void run();

    void handle_pdu(const pmt::pmt_t& pdu);

    void handle_stats_in(const pmt::pmt_t& pdu);

public:
    dsa_stats_impl(int period,
                   bool primary,
                   double tput_offered);

    ~dsa_stats_impl();

    void reset_stats(bool reset=true);

    double get_tput_offered(void) { return d_tput_offered; }

    void set_tput_offered(double tput_offered) { d_tput_offered = tput_offered; }

    // Overload gr::block start/stop to start internal stats thread
    bool start();
    bool stop();
};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_DSA_STATS_IMPL_H */
