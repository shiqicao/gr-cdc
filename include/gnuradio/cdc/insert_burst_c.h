/* -*- c++ -*- */
/*
 * Copyright 2023 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CDC_INSERT_BURST_C_H
#define INCLUDED_CDC_INSERT_BURST_C_H

#include <gnuradio/cdc/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace cdc {

/*!
 * \brief Inserts a burst of IQ samples into a stream of zeros.
 * \ingroup cdc
 *
 * Input:\n
 * PDU pair with a data segment of complex IQ samples 
 *
 * Output:\n
 * Stream of complex samples of current burst or zeros if no burst is present
 *
 * This block is a workaround to allow discontinuous bursts of IQ samples to
 * be provided to the bladeRF. If no burst is available the block will produce
 * zeros. 
 */
class CDC_API insert_burst_c : virtual public gr::sync_block {
public:
    typedef std::shared_ptr<insert_burst_c> sptr;

    static sptr make();
};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_INSERT_BURST_C_H */
