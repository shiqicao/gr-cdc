/* -*- c++ -*- */
/*
 * Copyright 2023 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CDC_TAG_TX_BURST_CC_IMPL_H
#define INCLUDED_CDC_TAG_TX_BURST_CC_IMPL_H

#include <gnuradio/cdc/tag_tx_burst_cc.h>

namespace gr {
namespace cdc {

class tag_tx_burst_cc_impl : public tag_tx_burst_cc {
private:
    const pmt::pmt_t d_lengthtag;
    int d_scalar;
    uint64_t d_eob_offset;

public:
    tag_tx_burst_cc_impl(const std::string& lengthtagname,
                         int scalar);
    ~tag_tx_burst_cc_impl();

    int work(int noutput_items,
             gr_vector_const_void_star &input_items,
             gr_vector_void_star &output_items) override;
};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_TAG_TX_BURST_CC_IMPL_H */
