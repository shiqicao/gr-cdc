/*
 * Copyright 2023 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr,cdc, __VA_ARGS__ )
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */


 
 static const char *__doc_gr_cdc_dsa_stats = R"doc()doc";


 static const char *__doc_gr_cdc_dsa_stats_dsa_stats_0 = R"doc()doc";


 static const char *__doc_gr_cdc_dsa_stats_dsa_stats_1 = R"doc()doc";


 static const char *__doc_gr_cdc_dsa_stats_make = R"doc()doc";


 static const char *__doc_gr_cdc_dsa_stats_reset_stats = R"doc()doc";


 static const char *__doc_gr_cdc_dsa_stats_set_tput_offered = R"doc()doc";


 static const char *__doc_gr_cdc_dsa_stats_get_tput_offered = R"doc()doc";

  
