/* -*- c++ -*- */
/*
 * Copyright 2023 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CDC_DSA_PU_SCENARIO_CC_H
#define INCLUDED_CDC_DSA_PU_SCENARIO_CC_H

#include <gnuradio/cdc/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace cdc {

/*!
 * \brief Implements primary user scenario control for CDC dynamic spectrum
 *        access project.
 * \ingroup cdc
 *
 */
class CDC_API dsa_pu_scenario_cc : virtual public gr::sync_block
{
public:
    typedef std::shared_ptr<dsa_pu_scenario_cc> sptr;

    /*!
     * \param samp_rate      Sample rate of incoming data streams
     * \param duration_ms    Duration (in ms) of scenario before randomly
     *                       selecting new scenario
     * \param random         Randomly select primary user scenario every
     *                      \p duration_ms
     * \param seed           Seed used in random scenario selection
     * \param scenario       Initial primary user scenario - bitmap of active
     *                       channels, e.g., for 2 PU channels SCENARIO = 0b10
     *                       indicates channel 1 is inactive and channel 2 is
     *                       active
     */
    static sptr make(int samp_rate,
                     float duration_ms,
                     bool random,
                     int seed = 0,
                     int scenario = 0);
    
    //! Set primary user scenario
    virtual void set_scenario(int scenario) = 0;

    //! Get current primary user scenario
    virtual int get_scenario(void) = 0;

    //! Set random selection of primary user scenarios
    virtual void set_random(bool random) = 0;

    //! Get random selection of primary user scenarios
    virtual bool get_random(void) = 0;
    
    //! Set duration (in ms) of random primary user scenarios
    virtual void set_duration_ms(float duration_ms) = 0;

    //! Get duration (in ms) of random primary user scenarios
    virtual float get_duration_ms(void) = 0;

};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_DSA_PU_SCENARIO_CC_H */
