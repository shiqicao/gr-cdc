/* -*- c++ -*- */
/*
 * Copyright 2023 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "dsa_stats_impl.h"
#include <gnuradio/io_signature.h>

using std::chrono::steady_clock;
using std::chrono::duration;

namespace gr {
namespace cdc {

dsa_stats::sptr dsa_stats::make(int period,
                                bool primary,
                                double tput_offered)
{
    return gnuradio::make_block_sptr<dsa_stats_impl>(
        period, primary, tput_offered);
}

dsa_stats_impl::dsa_stats_impl(int period,
                               bool primary,
                               double tput_offered)
    : gr::block("dsa_stats",
                gr::io_signature::make(0, 0, 0),
                gr::io_signature::make(0, 0, 0)),
      d_period(period),
      d_primary(primary),
      d_tput_offered(tput_offered)
{
    message_port_register_in(pmt::mp("pdu"));
    set_msg_handler(pmt::mp("pdu"),
                    [this](const pmt::pmt_t& msg) { this->handle_pdu(msg); });
    message_port_register_in(pmt::mp("stats_in"));
    set_msg_handler(pmt::mp("stats_in"),
                    [this](const pmt::pmt_t& msg) { this->handle_stats_in(msg); });
    message_port_register_out(pmt::mp("stats"));
}

dsa_stats_impl::~dsa_stats_impl() {}

bool dsa_stats_impl::start()
{
    d_finished = false;

    d_thread = std::shared_ptr<gr::thread::thread>(
        new gr::thread::thread(boost::bind(&dsa_stats_impl::run, this)));

    return block::start();
}

bool dsa_stats_impl::stop()
{
    // Shut down the thread
    {
        gr::thread::scoped_lock lock(d_mtx);
        d_finished = true;
    }
    d_thread->interrupt();
    d_thread->join();

    return block::stop();
}

void dsa_stats_impl::run()
{
    bool finished = false;
    reset_stats();
    
    while (!finished) {
        // elapsed time
        auto now = steady_clock::now();
        duration<double> dur_cur = (now - d_last_tp);
        duration<double> dur_avg = (now - d_start_tp);
        d_last_tp = now;

        // get current stats
        double bytes_new = 0;
        double tput_remote = 0;
        {
            gr::thread::scoped_lock lock(d_mtx);
            
            bytes_new = d_bytes_new;
            tput_remote = d_tput_remote;
            d_bytes_new = 0;
        }
        d_bytes += bytes_new;
        double tput_cur = 8.0*bytes_new / dur_cur.count();
        double tput_avg = 8.0*d_bytes / dur_avg.count();
        
        double tput_pu = d_primary ? tput_avg : tput_remote;
        double tput_su = d_primary ? tput_remote : tput_avg;
        double score = 0.0;
        if (tput_pu > 0) {
            score = exp(-10.0*(d_tput_offered - tput_pu)/tput_pu) * tput_su;
        }

        // publish stats message
        pmt::pmt_t stats = pmt::make_dict();
        stats = pmt::dict_add(stats,
                              pmt::mp("tput_cur"),
                              pmt::from_double(tput_cur));
        stats = pmt::dict_add(stats,
                              pmt::mp("tput_avg"),
                              pmt::from_double(tput_avg));
        stats = pmt::dict_add(stats,
                              pmt::mp("score"),
                              pmt::from_double(score));
        message_port_pub(pmt::mp("stats"), stats);
        
        // sleep for 'period' second
        boost::this_thread::sleep(
            boost::posix_time::seconds(static_cast<long>(d_period)));
    
        {
            gr::thread::scoped_lock lock(d_mtx);
            finished = d_finished;
        }
    }
}

void dsa_stats_impl::handle_pdu(const pmt::pmt_t& msg)
{
    gr::thread::scoped_lock lock(d_mtx);
    d_bytes_new += pmt::blob_length(pmt::cdr(msg));
}

void dsa_stats_impl::handle_stats_in(const pmt::pmt_t& msg)
{
    gr::thread::scoped_lock lock(d_mtx);
    d_tput_remote = pmt::to_double(pmt::dict_ref(msg,
                                                 pmt::mp("tput_avg"),
                                                 pmt::from_double(0.0)));
}

void dsa_stats_impl::reset_stats(bool reset)
{
    if (reset)
    {
        gr::thread::scoped_lock lock(d_mtx);

        d_bytes_new = 0.0;
        d_bytes = 0.0;
        d_start_tp = steady_clock::now();
        d_last_tp = d_start_tp;
    }
}

} /* namespace cdc */
} /* namespace gr */
